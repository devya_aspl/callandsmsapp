package com.callandsmsapp.models;

public class CallModel {

    String phoneNumber,country;

    public CallModel(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public CallModel(String phoneNumber, String country) {
        this.phoneNumber = phoneNumber;
        this.country = country;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
