package com.callandsmsapp.dbo;

public class ProjectConfigurationDbo {
    String timeId,call_disconnect_time,no_sms_send_each_time;

//    public ProjectConfigurationDbo(String call_disconnect_time, String no_sms_send_each_time, String timeId) {
//        this.call_disconnect_time = call_disconnect_time;
//        this.no_sms_send_each_time = no_sms_send_each_time;
//        this.timeId = timeId;
//    }


    public ProjectConfigurationDbo() {
    }

    public ProjectConfigurationDbo(String timeId, String call_disconnect_time, String no_sms_send_each_time) {
        this.timeId = timeId;
        this.call_disconnect_time = call_disconnect_time;
        this.no_sms_send_each_time = no_sms_send_each_time;
    }

    public String getCall_disconnect_time() {
        return call_disconnect_time;
    }

    public void setCall_disconnect_time(String call_disconnect_time) {
        this.call_disconnect_time = call_disconnect_time;
    }

    public String getNo_sms_send_each_time() {
        return no_sms_send_each_time;
    }

    public void setNo_sms_send_each_time(String no_sms_send_each_time) {
        this.no_sms_send_each_time = no_sms_send_each_time;
    }

    public String getTimeId() {
        return timeId;
    }

    public void setTimeId(String timeId) {
        this.timeId = timeId;
    }
}
