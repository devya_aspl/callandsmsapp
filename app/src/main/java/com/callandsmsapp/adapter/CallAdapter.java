package com.callandsmsapp.adapter;

import android.Manifest;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.callandsmsapp.MyReceiver;
import com.callandsmsapp.R;
import com.callandsmsapp.activities.CallActivity;
import com.callandsmsapp.models.CallModel;
import com.callandsmsapp.models.Country;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import static android.content.Context.ALARM_SERVICE;

public class CallAdapter extends RecyclerView.Adapter<CallAdapter.ViewHolder> {

   public ArrayList<CallModel> data = new ArrayList<>();
    public ArrayList<CallModel> selected_usersList=new ArrayList<>();
    Context context;
    CountDownTimer timer;

    public CallAdapter(Context context, ArrayList<CallModel> data,ArrayList<CallModel> selected_usersList) {
        this.context = context;
        this.data = data;
        this.selected_usersList = selected_usersList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new CallAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        final CallModel callModel = data.get(position);
        holder.textView.setText(callModel.getCountry());
        holder.tv_number.setText(callModel.getPhoneNumber());

        if(selected_usersList.contains(data.get(position)))
            holder.ll_call_list_item.setBackgroundColor(ContextCompat.getColor(context, R.color.list_item_selected_state));
        else
            holder.ll_call_list_item.setBackgroundColor(ContextCompat.getColor(context, R.color.list_item_normal_state));



        holder.ll_call_list_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phoneNumber = callModel.getPhoneNumber();
                phoneNumber = phoneNumber.replace(" ", "");
                phoneNumber = phoneNumber.replace("+", "");
                phoneNumber =  phoneNumber.substring(2);
                Log.e("callAdapter","Phone Number is: "+phoneNumber);
                makeCall(phoneNumber,position);
            }
        });
    }

    public boolean isPermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (context.checkSelfPermission(android.Manifest.permission.CALL_PHONE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("TAG", "Permission is granted");
                return true;
            } else {

                Log.v("TAG", "Permission is revoked");
                ActivityCompat.requestPermissions((CallActivity)context, new String[]{Manifest.permission.CALL_PHONE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("TAG", "Permission is granted");
            return true;
        }
    }


    private void makeCall(String phoneNumber,int position) {
//        Intent intent = new Intent(Intent.ACTION_DIAL);
//        intent.setData(Uri.parse("tel:"+phoneNumber));
//        context.startActivity(intent);

        //performs call
        if (!phoneNumber.equals("")) {

//            Intent intent = new Intent(context, MyReceiver.class);
//            PendingIntent pi = PendingIntent.getBroadcast(context, 0, intent, 0);
//            AlarmManager alarmManager = (AlarmManager)context.getSystemService(ALARM_SERVICE);
//            int milliseconds =  60 * 1000;
//
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
//                        alarmManager.setExact(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + milliseconds, pi);
//                    else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
//                alarmManager.setExact(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + milliseconds, pi);
//                    else
//                alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + milliseconds, pi);

//            alarmManager.setExact(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + milliseconds, pi);

            Intent intent2 = new Intent(Intent.ACTION_DIAL);
            intent2.setData(Uri.parse("tel:"+phoneNumber));
            context.startActivity(intent2);
            if(timer != null){
               timer.cancel();
               timer = null;
            }

            timer = new CountDownTimer(60000, 1000) {
                @Override
                public void onTick(long l) {
                    Log.e("seconds remaining : ", "seconds remaining : " + l / 1000);
                }

                @Override
                public void onFinish() {
                    Log.e("done!", "done!");
                    endCall(context);
                }
            }.start();
//
//          timer =  new CountDownTimer(120000, 1000) {
//
//                public void onTick(long millisUntilFinished) {
//                    Log.e("adapter","seconds remaining: " + millisUntilFinished / 1000);
//                    //here you can have your logic to set text to edittext
//                }
//
//                public void onFinish() {
//                    Log.e("adapter","done!");
////                    endCall(context);
////                    killCall(context);
//                    try {
//                        declinePhone(context);
//
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//
//            }.start();
//            timer.cancel();

        }


    }

    public void endCall(Context context) {
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        try {
            Class c = Class.forName(tm.getClass().getName());
            Method m = c.getDeclaredMethod("getITelephony");
            m.setAccessible(true);
            Object telephonyService = m.invoke(tm);

            c = Class.forName(telephonyService.getClass().getName());
            m = c.getDeclaredMethod("endCall");
            m.setAccessible(true);
            m.invoke(telephonyService);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }



//    public void endCall(Context context) {
//        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
//        try {
//            Class c = Class.forName(tm.getClass().getName());
//            Method m = c.getDeclaredMethod("getITelephony");
//            m.setAccessible(true);
//            Object telephonyService = m.invoke(tm);
//
//            c = Class.forName(telephonyService.getClass().getName());
//            m = c.getDeclaredMethod("endCall");
//            m.setAccessible(true);
//            m.invoke(telephonyService);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    private void declinePhone(Context context) throws Exception {

        try {

            String serviceManagerName = "android.os.ServiceManager";
            String serviceManagerNativeName = "android.os.ServiceManagerNative";
            String telephonyName = "com.android.internal.telephony.ITelephony";
            Class<?> telephonyClass;
            Class<?> telephonyStubClass;
            Class<?> serviceManagerClass;
            Class<?> serviceManagerNativeClass;
            Method telephonyEndCall;
            Object telephonyObject;
            Object serviceManagerObject;
            telephonyClass = Class.forName(telephonyName);
            telephonyStubClass = telephonyClass.getClasses()[0];
            serviceManagerClass = Class.forName(serviceManagerName);
            serviceManagerNativeClass = Class.forName(serviceManagerNativeName);
            Method getService = // getDefaults[29];
                    serviceManagerClass.getMethod("getService", String.class);
            Method tempInterfaceMethod = serviceManagerNativeClass.getMethod("asInterface", IBinder.class);
            Binder tmpBinder = new Binder();
            tmpBinder.attachInterface(null, "fake");
            serviceManagerObject = tempInterfaceMethod.invoke(null, tmpBinder);
            IBinder retbinder = (IBinder) getService.invoke(serviceManagerObject, "phone");
            Method serviceMethod = telephonyStubClass.getMethod("asInterface", IBinder.class);
            telephonyObject = serviceMethod.invoke(null, retbinder);
            telephonyEndCall = telephonyClass.getMethod("endCall");
            telephonyEndCall.invoke(telephonyObject);

        } catch (Exception e) {
            e.printStackTrace();
            Log.d("unable", "msg cant dissconect call....");

        }
    }

    public void filterList(ArrayList<CallModel> filteredData) {
        data = filteredData;
        notifyDataSetChanged();
    }

    public boolean killCall(Context context) {
        try {
            // Get the boring old TelephonyManager
            TelephonyManager telephonyManager =
                    (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

            // Get the getITelephony() method
            Class classTelephony = Class.forName(telephonyManager.getClass().getName());
            Method methodGetITelephony = classTelephony.getDeclaredMethod("getITelephony");

            // Ignore that the method is supposed to be private
            methodGetITelephony.setAccessible(true);

            // Invoke getITelephony() to get the ITelephony interface
            Object telephonyInterface = methodGetITelephony.invoke(telephonyManager);

            // Get the endCall method from ITelephony
            Class telephonyInterfaceClass =
                    Class.forName(telephonyInterface.getClass().getName());
            Method methodEndCall = telephonyInterfaceClass.getDeclaredMethod("endCall");

            // Invoke endCall()
            methodEndCall.invoke(telephonyInterface);

        } catch (Exception ex) { // Many things can go wrong with reflection calls
            Log.d("adapter","PhoneStateReceiver **" + ex.toString());
            return false;
        }
        return true;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView textView,tv_number;
        LinearLayout ll_call_list_item;
        ImageView iv_selected;

        public ViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.text_view_country);
            tv_number = (TextView) itemView.findViewById(R.id.tv_number);
//            iv_selected = (ImageView) itemView.findViewById(R.id.iv_selected);
            ll_call_list_item = (LinearLayout) itemView.findViewById(R.id.ll_call_list_item);
        }

    }
}
