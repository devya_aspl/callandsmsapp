package com.callandsmsapp.databases;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;


import com.callandsmsapp.dbo.ProjectConfigurationDbo;

import java.util.ArrayList;
import java.util.List;

public class DBHelperForConfigurationDetails extends SQLiteOpenHelper {


    // Database Version
    private static final int DATABASE_VERSION = 10678;

    Context context;

    // Database Name
    private static final String DATABASE_NAME = "callsms_configuration_db";
    private static final String TABLE_NAME = "appConfiguration";

    ProjectConfigurationDbo configurationDbo;

//    timerId,syncWithMobileData,autoSync,periodicity,chooseLanguage
    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "(timerId TEXT UNIQUE,call_disconnect_time TEXT,no_sms_send_each_time TEXT);";

    public DBHelperForConfigurationDetails(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        Log.e("database","project details table created");
        sqLiteDatabase.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);

        // Create tables again
        onCreate(db);
    }

//    timerId,syncWithMobileData,autoSync,periodicity,chooseLanguage
    public void insertConfigurationDetails(ProjectConfigurationDbo configurationDbo) {


        try {
//      timerId,syncWithMobileData,autoSync,periodicity,chooseLanguage
            SQLiteDatabase db = this.getWritableDatabase();


            Log.e("insert configuration :",
                    " timerId is:  "+ configurationDbo.getTimeId()
                            + "call_disconnect_time is: "+ configurationDbo.getCall_disconnect_time()
                            + "no_sms_send_each_time is: "+ configurationDbo.getNo_sms_send_each_time()
            );

//         timeId,call_disconnect_time,no_sms_send_each_time

            ContentValues values = new ContentValues();
//            timeId,call_disconnect_time,no_sms_send_each_time
            values.put("timerId", configurationDbo.getTimeId());
            values.put("call_disconnect_time", configurationDbo.getCall_disconnect_time());
            values.put("no_sms_send_each_time", configurationDbo.getNo_sms_send_each_time());

            long rowInserted =   db.insertWithOnConflict(TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
            if(rowInserted != -1)
                Toast.makeText(context, "Data Inserted Successfully", Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(context, "Something Wrong", Toast.LENGTH_SHORT).show();

            db.close();
//            Toast.makeText();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

//    public int getConfigurationCount() {
//        String countQuery = "SELECT  * FROM " + TABLE_NAME;
//        SQLiteDatabase db = this.getReadableDatabase();
//        Cursor cursor = db.rawQuery(countQuery, null);
//
//        int count = cursor.getCount();
//        cursor.close();
//
//
//        // return count
//        return count;
//    }

//    timeId,call_disconnect_time,no_sms_send_each_time
    public List<ProjectConfigurationDbo> getConfigurationDbo() {
        List<ProjectConfigurationDbo> configurationDboList = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NAME;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

//        timeId,call_disconnect_time,no_sms_send_each_time

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ProjectConfigurationDbo configurationDbo = new ProjectConfigurationDbo();
//              timeId,call_disconnect_time,no_sms_send_each_time
                configurationDbo.setTimeId(cursor.getString(cursor.getColumnIndex("timeId")));
                configurationDbo.setCall_disconnect_time(cursor.getString(cursor.getColumnIndex("call_disconnect_time")));
                configurationDbo.setNo_sms_send_each_time(cursor.getString(cursor.getColumnIndex("no_sms_send_each_time")));
                configurationDboList.add(configurationDbo);
            } while (cursor.moveToNext());
        }

        // close db connection
        db.close();

        // return notes list
        return configurationDboList;
    }

    public int getConfigurationCount() {
        String countQuery = "SELECT  * FROM " + TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();


        // return count
        return count;
    }
}
