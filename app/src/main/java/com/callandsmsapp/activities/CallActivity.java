package com.callandsmsapp.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.CountDownTimer;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.callandsmsapp.adapter.CallAdapter;
import com.callandsmsapp.listener.RecyclerItemClickListener;
import com.callandsmsapp.models.CallModel;
import com.callandsmsapp.adapter.CountryAdapter;
import com.callandsmsapp.adapter.CountryAdapter2;
import com.callandsmsapp.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CallActivity extends AppCompatActivity implements Spinner.OnItemSelectedListener{

    @BindView(R.id.rv_call_list)
    RecyclerView rv_call_list;

    @BindView(R.id.lv_names)
    ListView lv_names;

    @BindView(R.id.btn_sort)
    Button btn_sort;

    @BindView(R.id.btn_desc_sort)
    Button btn_desc_sort;

    @BindView(R.id.sp_sort_country)
    Spinner sp_sort_country;

    @BindView(R.id.sp_sort_tier)
    Spinner sp_sort_tier;

    @BindView(R.id.ll_list_selector)
    LinearLayout ll_list_selector;

    @BindView(R.id.tv_select_all)
    TextView tv_select_all;

    @BindView(R.id.iv_right)
    ImageView iv_right;

    ArrayAdapter<String> adapterChooseSortBy,adapterChooseSortByTier;

    CountryAdapter adapter;
    CountryAdapter2 adapter2;
    private boolean ascending = true;
    ArrayList<CallModel> callModelList = new ArrayList<>();
    ArrayList<CallModel> multiselect_list = new ArrayList<>();
    CallAdapter callAdapter;
    Context context;
    boolean isMultiSelect = false;

    private List<String> stringList;
    CountDownTimer timer;
    int position =0 ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call);
        context = CallActivity.this;
        isPermissionGranted();
        ButterKnife.bind(this);
        setUpList();

        rv_call_list.addOnItemTouchListener(new RecyclerItemClickListener(this, rv_call_list, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (isMultiSelect)
                    multi_select(position);
                else
                    Toast.makeText(getApplicationContext(), "Details Page", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onItemLongClick(View view, int position) {
                if (!isMultiSelect) {
                    multiselect_list = new ArrayList<CallModel>();
                    isMultiSelect = true;

                    ll_list_selector.setVisibility(View.VISIBLE);


//                    if (mActionMode == null) {
//                        mActionMode = startActionMode(mActionModeCallback);
//                    }
                }

                multi_select(position);
            }
        }));


//        callForSelectedNumbers(multiselect_list);
        setUpSpinners();

    }

    private void setUpSpinners() {
        List<String> spinnerChooseSortBy=  new ArrayList<String>();
        spinnerChooseSortBy.add("All Countries");
        spinnerChooseSortBy.add("India");
        spinnerChooseSortBy.add("America");
        spinnerChooseSortBy.add("Ukraine");
        spinnerChooseSortBy.add("USA");

        adapterChooseSortBy = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerChooseSortBy);

        adapterChooseSortBy.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//       Spinner sItems = (Spinner) findViewById(R.id.spinner1);
        sp_sort_country.setAdapter(adapterChooseSortBy);

        sp_sort_country.setOnItemSelectedListener(this);

        List<String> spinnerChooseSortByTier=  new ArrayList<String>();
        spinnerChooseSortByTier.add("All Tier");
        spinnerChooseSortByTier.add("Tier 1");
        spinnerChooseSortByTier.add("Tier 2");
        spinnerChooseSortByTier.add("Tier 3");
        spinnerChooseSortByTier.add("Tier 4");

        adapterChooseSortByTier = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerChooseSortByTier);

        adapterChooseSortByTier.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//       Spinner sItems = (Spinner) findViewById(R.id.spinner1);
        sp_sort_tier.setAdapter(adapterChooseSortByTier);

        sp_sort_tier.setOnItemSelectedListener(this);
    }

    private void setUpList() {
        rv_call_list.setLayoutManager(new LinearLayoutManager(context));

        callModelList = new ArrayList<CallModel>();
        callModelList.add(new CallModel("+91 7043169995","India"));
        callModelList.add(new CallModel("+91 8154936235","India"));
        callModelList.add(new CallModel("+91 8160058122","America"));
        callModelList.add(new CallModel("+91 8154936235","Ukraine"));
        callModelList.add(new CallModel("+91 8160058122","USA"));
        callModelList.add(new CallModel("+91 8154936235","India"));

        callAdapter = new CallAdapter(context,callModelList,multiselect_list);

        Log.e("Before Sorting:",callModelList.get(0).getCountry());

        rv_call_list.setAdapter(callAdapter);


//        Collections.sort(callModelList, new Comparator<CallModel>() {
//            public int compare(CallModel v1, CallModel v2) {
//                return v1.getCountry().compareTo(v2.getCountry());
//            }
//        });
//
//        Log.e("Sorting:",callModelList.get(0).getCountry());


//        callModelList.sort();
    }

    public void multi_select(int position) {
//        if (mActionMode != null) {
        if (multiselect_list.contains(callModelList.get(position)))
            multiselect_list.add(callModelList.get(position));
        else
            multiselect_list.add(callModelList.get(position));

        callForSelectedNumbers(multiselect_list);
        Log.e("activity","Selected List Size is:"+multiselect_list.size());

//            if (multiselect_list.size() > 0)
//                mActionMode.setTitle("" + multiselect_list.size());
//            else
//                mActionMode.setTitle("");

        refreshAdapter();

//        }
    }

    private void callForSelectedNumbers(final ArrayList<CallModel> multiselect_list) {


        Log.e("activity","First Phone Number: "+multiselect_list.get(position).getPhoneNumber());
            if(timer != null){
                timer.cancel();
                timer = null;
            }

            timer = new CountDownTimer(60000, 1000) {
                @Override
                public void onTick(long l) {
                    Log.e("seconds remaining : ", "seconds remaining : " + l / 1000);
                }

                @Override
                public void onFinish() {
                    Log.e("done!", "done!");
                    position = position+1;
                    Log.e("activity","Second Phone Number: "+multiselect_list.get(position).getPhoneNumber());
//                    endCall(context);
                }
            }.start();

//        while (i == multiselect_list.size())
//        {
//
//        }
    }

//    private void makeCall(String phoneNumber,int position) {
////        Intent intent = new Intent(Intent.ACTION_DIAL);
////        intent.setData(Uri.parse("tel:"+phoneNumber));
////        context.startActivity(intent);
//
//        //performs call
//        if (!phoneNumber.equals("")) {
//
////            Intent intent = new Intent(context, MyReceiver.class);
////            PendingIntent pi = PendingIntent.getBroadcast(context, 0, intent, 0);
////            AlarmManager alarmManager = (AlarmManager)context.getSystemService(ALARM_SERVICE);
////            int milliseconds =  60 * 1000;
////
////            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
////                        alarmManager.setExact(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + milliseconds, pi);
////                    else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
////                alarmManager.setExact(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + milliseconds, pi);
////                    else
////                alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + milliseconds, pi);
//
////            alarmManager.setExact(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + milliseconds, pi);
//
//            Intent intent2 = new Intent(Intent.ACTION_DIAL);
//            intent2.setData(Uri.parse("tel:"+phoneNumber));
//            context.startActivity(intent2);
//            if(timer != null){
//                timer.cancel();
//                timer = null;
//            }
//
//            timer = new CountDownTimer(60000, 1000) {
//                @Override
//                public void onTick(long l) {
//                    Log.e("seconds remaining : ", "seconds remaining : " + l / 1000);
//                }
//
//                @Override
//                public void onFinish() {
//                    Log.e("done!", "done!");
//                    endCall(context);
//                }
//            }.start();
////
////          timer =  new CountDownTimer(120000, 1000) {
////
////                public void onTick(long millisUntilFinished) {
////                    Log.e("adapter","seconds remaining: " + millisUntilFinished / 1000);
////                    //here you can have your logic to set text to edittext
////                }
////
////                public void onFinish() {
////                    Log.e("adapter","done!");
//////                    endCall(context);
//////                    killCall(context);
////                    try {
////                        declinePhone(context);
////
////                    } catch (Exception e) {
////                        e.printStackTrace();
////                    }
////                }
////
////            }.start();
////            timer.cancel();
//
//        }
//
//
//    }

//    String phoneNumber = callModel.getPhoneNumber();
//    phoneNumber = phoneNumber.replace(" ", "");
//    phoneNumber = phoneNumber.replace("+", "");
//    phoneNumber =  phoneNumber.substring(2);
//                Log.e("callAdapter","Phone Number is: "+phoneNumber);
//    makeCall(phoneNumber,position);

    public void refreshAdapter()
    {
        callAdapter.selected_usersList=multiselect_list;
        callAdapter.data=callModelList;
        callAdapter.notifyDataSetChanged();
    }


    public boolean isPermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.CALL_PHONE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("TAG", "Permission is granted");
                return true;
            } else {

                Log.v("TAG", "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("TAG", "Permission is granted");
            return true;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,String permissions[], int[] grantResults) {
        switch (requestCode) {

            case 1: {

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(context, "Permission granted", Toast.LENGTH_SHORT).show();
//                    makeCall();
                } else {
                    Toast.makeText(context, "Permission denied", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if (sp_sort_country.getSelectedItem().toString().equalsIgnoreCase("All Countries"))
        {
            setUpList();
        }
//        if (sp_sort_country.getSelectedItem().toString().equalsIgnoreCase("India"))
        else
        {
            filter(sp_sort_country.getSelectedItem().toString());
        }
//        else if (sp_sort_country.getSelectedItem().toString().equalsIgnoreCase("America"))
//        {
//            filter("America");
//        }
//
//        else if(sp_sort_country.getSelectedItem().toString().equalsIgnoreCase("Ukraine"))
//        {
//            filter("Ukraine");
//        }
//        else if (sp_sort_country.getSelectedItem().toString().equalsIgnoreCase("USA"))
//        {
//            filter("USA");
//        }
    }

    private void filter(String text) {

        try{
            ArrayList<CallModel> filteredList = new ArrayList<>();

            for (CallModel item : callModelList)
            {
                if (item.getCountry().toLowerCase().contains(text.toLowerCase()))
                {
                    filteredList.add(item);
                }
            }

            callAdapter.filterList(filteredList);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @OnClick(R.id.tv_select_all)
    public void onSelectedAll(View v)
    {
        iv_right.setVisibility(View.VISIBLE);
        if (isMultiSelect)
        multiselect_list.addAll(callModelList);
        refreshAdapter();
    }
}
