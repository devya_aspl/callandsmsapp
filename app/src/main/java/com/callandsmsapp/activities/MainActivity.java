package com.callandsmsapp.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.callandsmsapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.btn_call)
    Button btn_call;

    @BindView(R.id.btn_sms)
    Button btn_sms;

    @BindView(R.id.btn_configure)
    Button btn_configure;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_call)
    public void onCallClicked(View v)
    {
        Intent i = new Intent(MainActivity.this,CallActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.btn_sms)
    public void onSmSClicked(View v)
    {
        

    }

    @OnClick(R.id.btn_configure)
    public void onConfigurationClicked(View v)
    {
        Intent i = new Intent(MainActivity.this,ConfigurationActivity.class);
        startActivity(i);
    }

}
