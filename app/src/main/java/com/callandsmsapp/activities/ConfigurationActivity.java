package com.callandsmsapp.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.callandsmsapp.R;
import com.callandsmsapp.databases.DBHelperForConfigurationDetails;
import com.callandsmsapp.dbo.ProjectConfigurationDbo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ConfigurationActivity extends AppCompatActivity {

    @BindView(R.id.edt_call_seconds)
    EditText edt_call_seconds;

    @BindView(R.id.edt_send_numbers)
    EditText edt_send_numbers;

    @BindView(R.id.btn_save)
    Button btn_save;

    DBHelperForConfigurationDetails dbHelperConfig;
    public static final String TAG = ConfigurationActivity.class.getSimpleName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuration);
        ButterKnife.bind(this);
        dbHelperConfig = new DBHelperForConfigurationDetails(this);
    }

    @OnClick(R.id.btn_save)
    public void onClickSave(View v)  {

        if (validation()) {
            SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
//            Date currentFromApi = simpleDateFormat1.parse(Utilities.getGotCurrentDate((BaseActivity) getActivity()));
//            Log.e(TAG, "Api Date: " + currentFromApi);
            Long system_timestamp = System.currentTimeMillis();
            String system_str_date = getDate(system_timestamp);
            try {
                Date current_system_date = simpleDateFormat1.parse(system_str_date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Log.e(TAG, "Configuration Current Timestamp: " + system_timestamp);
            Log.e(TAG, "Configuration Current date: " + system_str_date);

            Random r = new Random();
            int randomNumber = r.nextInt(5);

            String random_timerId = randomNumber + "_" + system_timestamp;
//            timeId,call_disconnect_time,no_sms_send_each_time
            dbHelperConfig.insertConfigurationDetails(new ProjectConfigurationDbo(random_timerId,edt_call_seconds.getText().toString(),edt_send_numbers.getText().toString() ));
        }
//        else
//        {
//            Toast.makeText(this,"Please Enter Some Value",Toast.LENGTH_SHORT).show();
//        }
    }

    private String getDate(long time) {
        Date date = new Date(time); // *1000 is to convert seconds to milliseconds
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH); // the format of your date
        return sdf.format(date);
    }

    private boolean validation() {
        if(edt_call_seconds.getText().toString().trim().length() == 0){
            edt_call_seconds.setError("Please Enter Call disconnect time");
            edt_call_seconds.requestFocus();
//            Toast.makeText(this,)
//            Utilities.showSnackBarToast(lv_main,"Please Enter Licence Key");
//            edt_licence_id.setError("Please Enter Licence Key");
//            Utili.requestFocus();
            return false;
        }
        else if (edt_send_numbers.getText().toString().trim().length() == 0)
        {
            edt_send_numbers.setError("Please Enter Number of SMS send to each number");
            edt_send_numbers.requestFocus();
//            Utilities.showSnackBarToast(lv_main,"Please Enter Licence Code");
//            edt_licence_code.setError("Please Enter Licence Code");
//            edt_licence_code.requestFocus();
            return false;
        }
        else
        {
            return true;
        }
    }
}
